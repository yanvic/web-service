
# Projeto web services

O Projeto Web Services para Barbearia é uma plataforma online que visa facilitar o gerenciamento de uma barbearia, permitindo aos clientes marcar horários para cortes de cabelo e ao proprietário gerenciar esses agendamentos. Este projeto visa fornecer uma solução eficiente e conveniente para as operações diárias de uma barbearia, melhorando a experiência do cliente e simplificando o processo de agendamento.

## Autores

- [@yanvictor](https://www.github.com/yanvic)
- [@levichaves](https://www.github.com/levi.chavss)
- [@felipesousa](https://www.github.com/0felipesousa)


## Deploy

Para fazer o deploy desse projeto rode

```bash
  mvn deploy
```


## Funcionalidades

- Agendamento de Horário: Os clientes devem poder visualizar os horários disponíveis e agendar um horário para um corte de cabelo.
- Histórico e dúvidas: Os barbeiro podem configurar preços, horários e os clientes poderão ter acessos a essas informações para tirar dúvidas
- Interface multiplataforma (web e mobile)


## Instalação

Instale my-project com nvm

```bash
  nvm install my-project
  cd my-project
```
    
## Stack utilizada

**Front-end:** React

**Back-end:** Java, SpringBoot

**Mobile:** React Native